
class MySuite extends munit.FunSuite {
  test("case 1") {
    val maxWidth = 16
    val text = List("This", "is", "an", "example", "of", "text", "justification.")

    val obtained = justify(maxWidth, text)

    assertEquals(obtained, List("This    is    an","example  of text","justification.  "))
  }

  test("case 2") {
    val maxWidth = 16
    val text = List("What","must","be","acknowledgment","shall","be")

    val obtained = justify(maxWidth, text)

    assertEquals(obtained, List("What   must   be","acknowledgment  ","shall be        "))
  }
  
  test("case 3") {
    val maxWidth = 20
    val text = List("Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do")

    val obtained = justify(maxWidth, text)

    assertEquals(obtained, List("Science  is  what we","understand      well","enough to explain to","a  computer.  Art is","everything  else  we","do                  "))
  }
}
