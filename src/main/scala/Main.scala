@main def hello(): Unit = {
  val text = List("Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do")
  val obtained = justify(20, text)
  println(obtained)
}

def lengthOfLine(line: List[String]): Int = line.map(_.length).reduce((a,b) => a + b)

def parseLines(lengthLine: Int, words: List[String], currentLine: List[String] = Nil): List[List[String]] =
  words.headOption.fold(ifEmpty = List(currentLine))(
    wordCandidate => {
      val lineCandidate = if(currentLine.isEmpty) List(wordCandidate) else currentLine :+ s" $wordCandidate" 

      if(lengthOfLine(lineCandidate) > lengthLine) currentLine :: parseLines(lengthLine, words)
      else parseLines(lengthLine, words.tail, lineCandidate)
    }
  )

def addJustifySpaces(maxWidth: Int, line: List[String]): List[String] = {
  val (wordsToWork, lastElement) = if(line.length > 1) line.splitAt(line.length -1) else (line, Nil)
  wordsToWork.zipWithIndex.map((word, index) => {
    val spaceNeeded = maxWidth - lengthOfLine(line)
    val spaceToAdd = (spaceNeeded/wordsToWork.length).toInt + (if (spaceNeeded%wordsToWork.length >= index+1) 1 else 0)
    word + (" " * spaceToAdd)
  }) ++ lastElement
}

def addLeftSpaces(maxWidth: Int, line: List[String]): List[String] = 
  val (wordsToWork, lastElement) = line.splitAt(line.length -1)
  wordsToWork ++ lastElement
    .map(word => {
      val spaceToAdd = maxWidth - lengthOfLine(line)
      word + (" " * spaceToAdd)
    })
  
def justify(maxWidth: Int, words: List[String]): List[String] = {
  def lines = parseLines(maxWidth, words)
  val (toJustify, toLeft) = lines.splitAt(lines.length -1)
  val linesJustified = toJustify.map(line => addJustifySpaces(maxWidth, line)) ++ 
    toLeft.map(line => addLeftSpaces(maxWidth, line))
  linesJustified.map(_.mkString)
}
